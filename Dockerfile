FROM php:7.2-fpm
COPY index.php ./
CMD ["php", "-S", "0.0.0.0:8000"]
EXPOSE 8000
